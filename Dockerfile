FROM php:7.2-apache

ARG BUILD_ENV="PRODUCTION"

RUN apt-get update \
 && apt-get install -q -y git zlib1g-dev libmemcached-dev libicu-dev libxml2-dev ssmtp zip \
 && docker-php-ext-configure intl \
 && docker-php-ext-install iconv opcache intl bcmath exif fileinfo gettext pdo_mysql soap \
 && pecl install igbinary \
 && docker-php-ext-enable igbinary \
 && curl -fsSL 'https://pecl.php.net/get/memcached' -o memcached.tar.gz \
 && mkdir -p /tmp/memcached \
 && tar -xf memcached.tar.gz -C /tmp/memcached --strip-components=1 \
 && rm memcached.tar.gz \
 && docker-php-ext-configure /tmp/memcached --enable-memcached-igbinary --enable-memcached-json \
 && docker-php-ext-install /tmp/memcached \
 && rm -r /tmp/memcached \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer \
 && rm -rf /tmp/* /var/tmp/* \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && usermod -u 1000 www-data \
 && usermod -G staff www-data

RUN if [ "$BUILD_ENV" = "DEVELOPMENT" ]; then \
        pecl install xdebug-2.6.1; \
        docker-php-ext-enable xdebug; \
        echo "xdebug.remote_autostart = 0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
        echo "xdebug.remote_enable = 1"    >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
    fi;

RUN { \
        echo '[PHP]'; \
        echo 'expose_php = Off'; \
        echo 'error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT'; \
        echo 'log_errors = On'; \
        echo 'error_log = /dev/stderr'; \
        echo 'display_errors = Off'; \
        echo 'register_argc_argv = Off'; \
        echo 'enable_dl = Off'; \
        echo 'sendmail_path = "/usr/sbin/ssmtp -t"'; \
        echo '[Date]'; \
        echo 'date.timezone = "America/Sao_Paulo"'; \
        echo '[Session]'; \
        echo 'session.save_path = "/tmp"'; \
    } > /usr/local/etc/php/php.ini; \
# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
    { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=2'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini; \
	{ \
		echo 'FromLineOverride=YES'; \
        echo 'mailhub=:587'; \
        echo 'rewriteDomain='; \
        echo 'AuthUser='; \
        echo 'AuthPass='; \
        echo 'UseTLS=YES'; \
        echo 'UseSTARTTLS=YES'; \
	} > /etc/ssmtp/ssmtp.conf

RUN a2enmod rewrite headers remoteip expires \
 && sed -i 's/ServerAdmin webmaster@localhost/ServerAdmin suporte@addpix.com.br/g' /etc/apache2/sites-available/000-default.conf \
 && sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf \
 && sed -i '/<\/VirtualHost>/i RemoteIPHeader X-Forwarded-For' /etc/apache2/sites-available/000-default.conf \
 && sed -i 's/\(LogFormat "\)%h\( %l %u %t \\"%r\\" %>s %O \\"%{Referer}i\\" \\"%{User-Agent}i\\"" combined\)/\1%a\2/g' /etc/apache2/apache2.conf \
 && echo "AllowEncodedSlashes On" >> /etc/apache2/apache2.conf \
 && sed -i 's!ServerTokens OS!ServerTokens Prod!g' /etc/apache2/conf-enabled/security.conf \
 && sed -i 's!ServerSignature On!ServerSignature Off!g' /etc/apache2/conf-enabled/security.conf

EXPOSE 80 443

WORKDIR /var/www
COPY --chown=www-data:www-data . /var/www
